class Menu {

    constructor() {
        this.el = document.getElementById('module-menu-mobile');
        this.init();
    }

    init() {
        const $this = this;
        const open = function onOpen() {
            $this.onAfterOpen();
        }
        const close = function onClose() {
            $this.onAfterClose();
        }

        this.onToggle(open, close);

    }

    onToggle(open, close) {
        let state = false;
        if (document.body.classList.contains('uk-offcanvas-container')) {
            let state = document.body.classList.contains('uk-offcanvas-container');
        }
        const observer = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {
                if (mutation.attributeName == "class") {
                    let currentClassState = mutation.target.classList.contains('uk-offcanvas-container');
                    if (state !== currentClassState) {
                        state = currentClassState;
                        if (currentClassState) {
                            open();
                        }
                        else {
                            close();
                        }
                    }
                }
            });
        });
        observer.observe(document.body, { attributes: true });
    }
    onAfterOpen() {
        const $this = this;
        $this.el.querySelectorAll('.uk-nav').forEach(function (nav) {
            nav.classList.add('uk-nav-parent-icon')

        });
        const parents = this.el.querySelectorAll('.uk-parent');
        parents.forEach(function (parent) {
            const submenu = parent.querySelector('.uk-nav-sub');
            submenu.setAttribute('hidden', 'true');
            submenu.classList.add('uk-dropdown-nav');
            const menu = parent.firstChild;
            menu.href = "#";
            menu.setAttribute('uk-toggle', "target: + .uk-nav-sub; animation: uk-animation-slide-right;");
        });
    }
    onAfterClose() {

    }

}

export default Menu;