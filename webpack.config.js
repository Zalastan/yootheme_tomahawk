const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const path = require('path');

const env = process.env.NODE_ENV;

module.exports = {
	mode: env || 'development', // on définit le mode en fonction de la valeur de NODE_ENV
	entry: './src/index.js',
	output: {
		filename: 'custom.js',
		path: path.resolve(__dirname, './js'),
	},
	module: {
		rules: [
			{
				test: /\.m?js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env']
					}
				}

			},
			{
				test: /\.sc|ass$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
					},
					{
						loader: "css-loader",
						options: {
							importLoaders: 1
						}
					},
					{
						loader: 'postcss-loader',
						options: {
							sourceMap: true,
							config: {
								ctx: {
									cssnano: {},
									autoprefixer: {}
								}
							}
						}
					},
					{
						loader: "sass-loader",
						options: {
							sourceMap: true, // il est indispensable d'activer les sourcemaps pour que postcss fonctionne correctement
						}
					},
				]
			},
			{
				test: /\.(eot|ttf|woff|woff2)$/,
				loader: 'file-loader',
				options: {
					name: '../fonts/[name].[ext]'
				}
			},
		]
	},
	plugins: [
		new MiniCssExtractPlugin({
			// filename: "main.[contenthash].css",
			filename: "../css/custom.css",
			chunkFilename: "[id].css",
		}),
		new CleanWebpackPlugin()
	]
}; 
